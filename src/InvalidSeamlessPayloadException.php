<?php

namespace Token;

use UnexpectedValueException;

class InvalidSeamlessPayloadException extends UnexpectedValueException
{
}
