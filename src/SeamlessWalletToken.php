<?php

namespace Token;

/**
 * Class SeamlessWalletToken
 * @package XgToken
 */
class SeamlessWalletToken
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * SeamlessWalletToken constructor.
     * @param string $key agentID
     * @param string $secret agentKey
     */
    public function __construct(string $key, string $secret)
    {
        if (empty($key) || empty($secret)) {
            throw new InvalidKeyOrSecretException('The key or the secret should not be empty');
        }
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * @param string $payload JSON string
     * @param string $token X-API-TOKEN
     * @return bool|InvalidJsonStringException
     */
    public function validate(string $payload, string $token)
    {
        $expectToken = $this->generateToken($payload);
        return $expectToken === $token;
    }

    /**
     * @param $payload
     * @return string|InvalidJsonStringException
     */
    public function generateToken(string $payload)
    {
        if ($payload !== $this->reformatPayload($payload)) {
            throw new InvalidSeamlessPayloadException('The format error of payload');
        }

        return strtolower(base64_encode(hex2bin(sha1($this->key . $payload . $this->secret))));
    }

    /**
     * @param string $payload JSON string
     * @return string|string[]|InvalidJsonStringException reformatted JSON string
     */
    public function reformatPayload(string $payload)
    {
        $params = json_decode($payload, true);
        if (is_null($params) && json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidJsonStringException('The payload is invalid json string');
        }
        $params = $this->handleReformation($params);

        //remove labels from number("%100.0000^" => 100.0000)
        return str_replace(
            '"%',
            '',
            str_replace('^"', '', json_encode($params, JSON_UNESCAPED_SLASHES))
        );
    }

    /**
     * @param array $params
     * @return array
     */
    private function handleReformation(array $params): array
    {
        ksort($params);
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $params[$key] = $this->handleReformation($value);
            }

            if (is_numeric($value) && !in_array($key, ['run', 'round', 'wagerId'])) {
                //should be 4th decimal place
                $newFormat = number_format($value, 4, '.', '');

                //add labels to number("100.0000" => "%100.0000^")
                $params[$key] = '%' . $newFormat . '^';
            }
        }

        return $params;
    }
}
