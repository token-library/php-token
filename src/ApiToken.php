<?php

namespace Token;

use DateTime;
use DateTimeZone;

/**
 * Class ApiToken
 * @package Token
 */
class ApiToken
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * ApiToken constructor.
     * @param string $key $key agentID
     * @param string $secret $secret agentKey
     */
    public function __construct(string $key, string $secret)
    {
        if (empty($key) || empty($secret)) {
            throw new InvalidKeyOrSecretException('The key or the secret should not be empty');
        }
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * @param string $payload
     * @param string $key
     * @return bool|InvalidJsonStringException
     */
    public function validate(string $payload, string $key)
    {
        $expectKey = $this->generateKey($payload);
        $expectHash = (substr(substr($expectKey, 6, strlen($expectKey)), 0, -6));

        $hash = (substr(substr($key, 6, strlen($key)), 0, -6));
        return $expectHash === $hash;
    }

    /**
     * @param string $payload JSON string
     * @return string|InvalidJsonStringException
     */
    public function generateKey(string $payload)
    {
        $paramsString = $this->generateParamsString($payload);
        return $this->getRandomString(6) . md5($paramsString . $this->getKeyG()) . $this->getRandomString(6);
    }

    protected function generateParamsString(string $payload)
    {
        $params = json_decode($payload, true);
        if (is_null($params) && json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidJsonStringException('The payload is invalid json string');
        }
        $requestData = array_map([$this, 'convertBooleanToString'], $params);
        return urldecode(http_build_query($requestData));
    }

    /**
     * http_build_query 會把 true 轉成 1, false 轉成 0,
     * 先把原本 request 的 true 轉成 'true', false 轉成 'false' 再做 http_build_query
     * @param $value
     * @return string
     */
    private function convertBooleanToString($value): string
    {
        if (is_bool($value)) {
            return ($value) ? 'true' : 'false';
        } else {
            return $value;
        }
    }

    protected function getKeyG(): string
    {
        $dateTime = new DateTime();
        $day = $dateTime->setTimezone(new DateTimeZone('-0400'))->format('ymj');
        return md5($day . $this->key . $this->secret);
    }

    private function getRandomString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }
}
