<?php

namespace Token;

use UnexpectedValueException;

class InvalidJsonStringException extends UnexpectedValueException
{
}
