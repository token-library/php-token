# PHP-TOKEN

A simple library to validate and generate token in PHP

## Installation

Use composer to manage your dependencies and download this library 

```
composer config repositories.php-token composer https://gitlab.com/api/v4/group/51903610/-/packages/composer/
composer require token-library/php-token 
```

## Example

### Seamless 1.1 Token

The following example would help you to validate or generate the `X-API-TOKEN`. The token generation rule refers to the [Seamless Wallet 1.1 Doc](https://github.com/jacky5823a/docs/blob/master/SeamlessWalletAPI1.x/SeamlessWallet1.1.md)

#### Validate X-API-TOKEN

```php
require __DIR__.'/vendor/autoload.php';

use Token\SeamlessWalletToken; 

$key = '<your agent ID>';
$secret = '<your agent key>';
$seamlessWalletToken = new SeamlessWalletToken($key, $secret);

$payload = '{"requestId":"test-123","user":"member"}';
$token = 'the-X-API-TOKEN-form-header';
$seamlessWalletToken->validate($payload, $token);
```

#### Generate X-API-TOKEN

```php
require __DIR__.'/vendor/autoload.php';

use Token\SeamlessWalletToken;

$key = '<your agent ID>';
$secret = '<your agent key>';
$seamlessWalletToken = new SeamlessWalletToken($key, $secret);

$payload = '{"requestId":"test-123","status":"ok","user":"member","currency":"USD","balance":100}';

// The reformatPayload method would help you to reformat payload to match the rules, for example: reformat the number to 4 decimal places number
// So the payload after reformatting would be '{"balance":100.0000,"currency":"USD","requestId":"test-123","status":"ok","user":"member"}'
$reformatPayload = $seamlessWalletToken->reformatPayload($payload);

$token = $seamlessWalletToken->generateToken($reformatPayload);

http_response_code(200);
header('Content-Type: application/json');
header('X-API-KEY: ' . $key);
header('X-API-TOKEN: ' . $token);
echo $reformatPayload;
```

### Api Key(For keno-api)

The following example would help you to generate the `Key` in request parameters. The token generation rule refers to the [Encryption Flow](https://github.com/jacky5823a/docs/blob/master/AccountingPlatformAPI/encryption-en.md)

#### Generate key

```php
require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;
use Token\ApiToken;

$key = '<your agent ID>';
$secret = '<your agent key>';
$apiToken = new ApiToken($key, $secret);

$params = [
    'AgentId' => $key,
    'Account' => 'member',
    'LimitStake' => '1,2,3'
];
$payload = json_encode($params);
$params['Key'] = $apiToken->generateKey($payload);

// Send request
$client = new Client(['base_uri' => THE_SERVER_HOST]);
$client->request('POST', '/api-path', ['json' => $params]);
```

### HMAC Signature(For v2 api)

The following example would help you to generate the `Signature` in the request header.

#### Query String(GET)
```php
require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;
use Token\ApiV2Signature;

$agentId = '<your agent ID>';
$agentKey = '<your agent key>';
$apiV2Signature = new ApiV2Signature($agentId, $agentKey);

$params = [
    'account' => 'member',
    'currency' => 'USD',
    'tableLimitIds' => [88, 89, 90]
];
$payload = http_build_query($params);
$requestTimestamp = times();
$signature = $apiV2Signature->generateSignature($payload, $requestTimestamp);

// Send request
$client = new Client(['base_uri' => THE_SERVER_HOST]);
$client->request('GET', '/api-path', [
    'headers' => [
        'X-Agent-Id' => $agentId,
        'X-Agent-Timestamp' => $requestTimestamp,
        'X-Agent-Signature' => $signature,
    ]       
    'query' => $params
]);
```

#### Request Body(POST, PATCH)
```php
require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;
use Token\ApiV2Signature;

$agentId = '<your agent ID>';
$agentKey = '<your agent key>';
$apiV2Signature = new ApiV2Signature($agentId, $agentKey);

$params = [
    'account' => 'member',
    'currency' => 'USD',
    'tableLimitIds' => [88, 89, 90]
];
$payload = json_encode($params);
$requestTimestamp = times();
$signature = $apiV2Signature->generateSignature($payload, $requestTimestamp);

// Send request
$client = new Client(['base_uri' => THE_SERVER_HOST]);
$client->request('POST', '/api-path', [
    'headers' => [
        'X-Agent-Id' => $agentId,
        'X-Agent-Timestamp' => $requestTimestamp,
        'X-Agent-Signature' => $signature,
    ]       
    'json' => $params
]);
```

## Tests

```
composer install
./vendor/bin/phpunit
```

## License

Please see [MIT License File](./LICENSE) for more information.
