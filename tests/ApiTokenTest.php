<?php

namespace Token\Tests;

use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;
use Token\ApiToken;
use Token\InvalidJsonStringException;
use Token\InvalidKeyOrSecretException;

class ApiTokenTest extends TestCase
{
    private $key = 'test-key';
    private $secret = 'test-secret';
    private $payload = '{"key1":"value","key2":true,"key3":false,"key4":12.3, "key5":"https://test.com"}';

    /**
     * @dataProvider constructForInvalidKeyOrSecretDataProvider
     *
     * @param string $key
     * @param string $secret
     */
    public function testConstructForInvalidKeyOrSecret(string $key, string $secret)
    {
        $this->expectException(InvalidKeyOrSecretException::class);
        $this->expectExceptionMessage('The key or the secret should not be empty');
        new ApiToken($key, $secret);
    }

    public function constructForInvalidKeyOrSecretDataProvider()
    {
        return [
            'key is empty string' => ['', $this->secret],
            'secret is empty string' => [$this->key, ''],
        ];
    }

    public function testValidateTrue()
    {
        $key = 'abcdef' . $this->getExpectHash() . '123456';
        $apiToken = new ApiToken($this->key, $this->secret);
        $this->assertTrue($apiToken->validate($this->payload, $key));
    }

    public function testValidateFalse()
    {
        $apiToken = new ApiToken($this->key, $this->secret);
        $this->assertFalse($apiToken->validate($this->payload, 'wrong-key'));
    }

    /**
     * @dataProvider generateKeyForInvalidPayloadDataProvider
     * @param $payload
     */
    public function testGenerateKeyForInvalidPayload($payload)
    {
        $this->expectException(InvalidJsonStringException::class);
        $this->expectExceptionMessage('The payload is invalid json string');
        $apiToken = new ApiToken($this->key, $this->secret);
        $apiToken->generateKey($payload);
    }

    public function generateKeyForInvalidPayloadDataProvider()
    {
        return [
            'payload is empty string' => [''],
            'payload is not json format' => ['{"key"=>"value"}'],
        ];
    }

    public function testGenerateKeyIsExpected()
    {
        $apiToken = new ApiToken($this->key, $this->secret);
        $key = $apiToken->generateKey($this->payload);
        $hash = (substr(substr($key, 6, strlen($key)), 0, -6));
        $this->assertEquals($this->getExpectHash(), $hash);
    }

    private function getExpectHash()
    {
        $dateTime = new DateTime();
        $day = $dateTime->setTimezone(new DateTimeZone('-0400'))->format('ymj');
        $keyG = md5($day . $this->key . $this->secret);
        return md5('key1=value&key2=true&key3=false&key4=12.3&key5=https://test.com' . $keyG);
    }
}
