<?php

namespace Token\Tests;

use PHPUnit\Framework\TestCase;
use Token\ApiV2Signature;
use Token\InvalidKeyOrSecretException;
use Token\InvalidRequestTimestamp;

class ApiV2SignatureTest extends TestCase
{
    private $key = 'test-key';
    private $secret = 'test-secret';
    private $payload = '{"key1":"value","key2":true,"key3":false,"key4":12.3,"key5":"https://test.com"}';

    /**
     * @dataProvider constructForInvalidKeyOrSecretDataProvider
     *
     * @param string $key
     * @param string $secret
     */
    public function testConstructForInvalidKeyOrSecret(string $key, string $secret)
    {
        $this->expectException(InvalidKeyOrSecretException::class);
        $this->expectExceptionMessage('The key or the secret should not be empty');
        new ApiV2Signature($key, $secret);
    }

    public function constructForInvalidKeyOrSecretDataProvider()
    {
        return [
            'key is empty string' => ['', $this->secret],
            'secret is empty string' => [$this->key, ''],
        ];
    }

    public function testInvalidRequestUnixTimestamp()
    {
        $timestamp = time() - (15 * 60 + 1);
        $this->expectException(InvalidRequestTimestamp::class);
        $this->expectExceptionMessage('The request unix timestamp is invalid');
        $apiV2Signature = new ApiV2Signature($this->key, $this->secret);
        $apiV2Signature->generateSignature($this->payload, $timestamp);
    }

    public function testValidateWithCorrectSignature()
    {
        $apiV2Signature = new ApiV2Signature($this->key, $this->secret);
        $signature = 'equAw0DN1WWJxcM8OBd53/RfIpkRdhkhujjXa/6r+xs=';
        $this->assertTrue($apiV2Signature->validate($this->payload, 9876543210, $signature));
    }

    public function testValidateWithWrongSignature()
    {
        $apiV2Signature = new ApiV2Signature($this->key, $this->secret);
        $this->assertFalse($apiV2Signature->validate($this->payload, 9876543210, 'wrong-sign'));
    }
}
