<?php

namespace Token\Tests;

use PHPUnit\Framework\TestCase;
use Token\InvalidJsonStringException;
use Token\InvalidKeyOrSecretException;
use Token\InvalidSeamlessPayloadException;
use Token\SeamlessWalletToken;

class SeamlessWalletTokenTest extends TestCase
{
    private $key = 'a778ec58-b9c2-419f-91dc-b0c1ed10873e';
    private $secret = '10b8ac75-4cac-4a77-a921-9a7c51ae1db6';
    // phpcs:disable Generic.Files.LineLength.TooLong
    private $payload = '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830.0000,"currency":"NTD","user":"zaqx1994"}],"status":"ok"}';
    // phpcs:enable
    private $expectToken = 'dvzaj57uxbmbeo2escgmynmzihq=';

    /**
     * @dataProvider constructForInvalidKeyOrSecretForDataProvider
     *
     * @param $key
     * @param $secret
     */
    public function testConstructForInvalidKeyOrSecret($key, $secret)
    {
        $this->expectException(InvalidKeyOrSecretException::class);
        $this->expectExceptionMessage('The key or the secret should not be empty');
        new SeamlessWalletToken($key, $secret);
    }

    public function constructForInvalidKeyOrSecretForDataProvider()
    {
        return [
            'key is empty string' => ['', $this->secret],
            'secret is empty string' => [$this->key, ''],
        ];
    }

    public function testValidateTrue()
    {
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $this->assertTrue($seamlessWalletToken->validate($this->payload, $this->expectToken));
    }

    public function testValidateFalse()
    {
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $this->assertFalse($seamlessWalletToken->validate($this->payload, 'wrong-token'));
    }

    /**
     * @dataProvider reformatForInvalidJsonStringDataProvider
     * @param $payload
     */
    public function testReformatForInvalidJsonString($payload)
    {
        $this->expectException(InvalidJsonStringException::class);
        $this->expectExceptionMessage('The payload is invalid json string');
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $seamlessWalletToken->reformatPayload($payload);
    }

    public function reformatForInvalidJsonStringDataProvider()
    {
        return [
            'payload is empty string' => [''],
            'payload is not json format' => ['{"key"=>"value"}'],
        ];
    }

    /**
     * @dataProvider reformatPayloadDataProvider
     *
     * @param $payload
     * @param $expectPayload
     */
    public function testReformatPayloadIsExpected($payload, $expectPayload)
    {
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $this->assertEquals($expectPayload, $seamlessWalletToken->reformatPayload($payload));
    }

    public function reformatPayloadDataProvider()
    {
        return [
            'balance is string case' => ['{"balance":"100"}', '{"balance":100.0000}'],
            'balance is float case' => ['{"balance":100.1}', '{"balance":100.1000}'],
            'number is int case' => ['{"balance":100}', '{"balance":100.0000}'],
            'number has 5th decimal place case' => ['{"balance":100.12345}', '{"balance":100.1235}'],
            'alphabetical order case for key' => [
                '{"c":"100.12345","a":"test","b":true}',
                '{"a":"test","b":true,"c":100.1235}'
            ],
            'alphabetical order case if value is object array' => [
                '{"list":[{"c":"100.12345","a":"test","b":true}]}',
                '{"list":[{"a":"test","b":true,"c":100.1235}]}'
            ],
            'do not add escaped slashes(2022\/01\/25) to time format' => [
                '{"betTime":"2022/01/25 13:35:59"}',
                '{"betTime":"2022/01/25 13:35:59"}'
            ],
            'do not reformat to 4th decimal place number when key = run, round, wagerId' => [
                '{"run":4,"round":99,"wagerId":1000,"others":10}',
                '{"others":10.0000,"round":99,"run":4,"wagerId":1000}'
            ],
            'release all number labels' => [
                '{"settleUserBalanceList":[{"balance":2},{"balance":2},{"balance":2}]}',
                '{"settleUserBalanceList":[{"balance":2.0000},{"balance":2.0000},{"balance":2.0000}]}'
            ],
        ];
    }

    /**
     * @dataProvider generateTokenForInvalidPayloadDataProvider
     * @param string $payload
     */
    public function testGenerateTokenForInvalidPayload($payload)
    {
        $this->expectException(InvalidSeamlessPayloadException::class);
        $this->expectExceptionMessage('The format error of payload');
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $seamlessWalletToken->generateToken($payload);
    }

    public function generateTokenForInvalidPayloadDataProvider()
    {
        // phpcs:disable Generic.Files.LineLength.TooLong
        return [
            'balance has no 4th decimal place' => [
                '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830,"currency":"NTD","user":"zaqx1994"}],"status":"ok"}'
            ],
            'there is no alphabetical order' => [
                '{"status":"ok", "requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830.0000,"currency":"NTD","user":"zaqx1994"}]}'
            ],
            'there is no alphabetical order in settleUserBalanceList' => [
                '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"currency":"NTD","balance":3830.0000,"user":"zaqx1994"}],"status":"ok"}'
            ]
        ];
        // phpcs:enable
    }

    public function testGenerateTokenIsExpected()
    {
        $seamlessWalletToken = new SeamlessWalletToken($this->key, $this->secret);
        $this->assertEquals($this->expectToken, $seamlessWalletToken->generateToken($this->payload));
    }
}
